import React, {Component} from "react";
import PropTypes from 'prop-types';

class MainPage extends Component {
	
	constructor(props){
		super(props);
		this.state = {
			symbols:['USD', 'EUR']
		};
		this.renderCurrencyRow = this.renderCurrencyRow.bind(this);
		this.getCurrentRates = this.getCurrentRates.bind(this);
		this.addCurrency = this.addCurrency.bind(this);
		this.getCurrentRates(this.state.symbols);
	}
	
	componentDidMount() {setInterval(() => this.getCurrentRates(this.state.symbols), 10000);}
	
	componentWillReceiveProps(nextProps){
		if(nextProps.updatedRate !== this.props.updatedRate){
			this.replaceRates(nextProps.updatedRate)
		}
	}
	
	static getCurrentData(){
		let today = new Date();
		let day = today.getDate();
		let month = today.getMonth()+1; //January is 0!
		let year = today.getFullYear();
		if(day<10) {day ='0'+day}
		if(month<10) {month = '0'+month}
		return day + '.' + month + '.' + year;
	}
	
	getCurrentRates(symbols){this.props.getRates(symbols);}
	
	updateRate(symbol){this.props.updateRate(symbol);}
	
	replaceRates(rate){
		let updatedKey = Object.keys(rate)[0];
		return Object.keys(this.props.rates).map((key) => {
			if(key === updatedKey && this.props.rates[key] !== rate[updatedKey]){
				let cloneObj = {...this.props.rates};
				cloneObj[key]= rate[updatedKey];
				this.props.loadRates(cloneObj);
				}
				return null
			}
		)
	}
	
	addCurrency(currency){
		let cur = [...this.state.symbols];
		cur.push(currency);
		this.setState({
			...this.state,
			symbols: cur,
		}, () => this.getCurrentRates(this.state.symbols))
	}
	
	renderCurrencyRow(){
		const {rates} = this.props;
		if((Object.keys(rates).length)){
			return (Object.keys(rates).map((key) => (
				<div key={key} className="currency-row">
					<span className="currency-row-text">{key}: {rates[key]}</span>
					<button
						onClick={() => this.updateRate(key)}
						className="currency-row-btn"
					>
						GET CURRENT RATE
					</button>
				</div>
			
			)))
		}
	}
	
	render() {
		const {renderCurrencyRow, addCurrency} = this;
		const currentData = MainPage.getCurrentData();
		return (
			<div className='wrapper'>
				<h1 className="title">Rates on {currentData}</h1>
				{renderCurrencyRow()}
				<button
					className="currency-row-btn"
					onClick={() => addCurrency('CAD')}
				>ADD CAD</button>
				<button
					className="currency-row-btn"
					onClick={() => addCurrency('JPY')}
				>ADD JPY</button>
			</div>
		
		);
	}
}

MainPage.propTypes = {
	updatedRate:PropTypes.object.isRequired,
	rates:PropTypes.object.isRequired,
	updateRate: PropTypes.func.isRequired,
	getRates: PropTypes.func.isRequired
};
export default MainPage;

