import {connect} from "react-redux";
import MainPage from "./MainPage";
import { getRates, updateRate, loadRates }  from "../../thunks/MainThunk";

const mapDispatchToProps = dispatch => ({
	getRates: (symbols) => dispatch(getRates(symbols)),
	updateRate: (symbols) => dispatch(updateRate(symbols)),
	loadRates: (rates) => dispatch(loadRates(rates)),
});

const mapStateToProps = state => ({
	loading : state.main.loading,
	rates : state.main.rates,
	updatedRate: state.main.updatedRate
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);

