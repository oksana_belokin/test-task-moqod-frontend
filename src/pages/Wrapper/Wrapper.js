import React, {Component} from "react";
import { Switch, Route } from 'react-router-dom';

import MainContainer from '../Main/MainContainer';

class Wrapper extends Component {
	
	render() {
		
		return (
			<main>
				<Switch>
					<Route exact path='/' component={MainContainer}/>
				</Switch>
			</main>
		)
		
	}
}

export default Wrapper
