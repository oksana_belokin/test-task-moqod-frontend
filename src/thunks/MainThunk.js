import axios from 'axios';
const API_PATH = "https://api.exchangeratesapi.io/latest?base=RUB";

export const LOAD_RATES = 'LOAD_RATES';
export const UPDATE_RATE = 'UPDATE_RATE';
export const LOADING = 'LOADING';

export function ratesIsLoading(bool) {
	return {
		type: LOADING,
		payload: bool
	};
}

export function loadRates(rates) {
	return {
		type: LOAD_RATES,
		payload: rates
	};
}

export function updateRateAction(rates) {
	return {
		type: UPDATE_RATE,
		payload: rates
	};
}

export const getRates = (symbols) => {
	return (dispatch) => {
		dispatch(ratesIsLoading(true));
		axios.get(`${API_PATH}&symbols=${symbols}`).then((rates) => {
				dispatch(ratesIsLoading(false));
				dispatch(loadRates(rates.data.rates));
			}
		)
	};
};

export const updateRate = (symbols) => {
	console.log(symbols);
	return (dispatch) => {
		dispatch(ratesIsLoading(true));
		axios.get(`${API_PATH}&symbols=${symbols}`).then((rates) => {
				dispatch(ratesIsLoading(false));
				dispatch(updateRateAction(rates.data.rates));
			}
		)
	};
};