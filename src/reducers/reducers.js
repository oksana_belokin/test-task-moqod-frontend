import main from './main.reducer';
import {combineReducers} from 'redux';

export default combineReducers({
	main
});