import {LOAD_RATES, LOADING, UPDATE_RATE} from '../thunks/MainThunk'

const initState = {
	rates:{},
	updatedRate:{},
	loading: false
};

export default (state = initState, action) => {
	console.log(action);
	switch (action.type) {
		case LOAD_RATES:
			return {
				...state,
				rates: action.payload
			};
		
		case UPDATE_RATE:
			return {
				...state,
				updatedRate: action.payload
			};
		case LOADING:
			return {
				...state,
				loading: action.payload
			};
		
		default:
			return state;
	}
};