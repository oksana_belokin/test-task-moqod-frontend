import React, { Component } from 'react';
import './styles/scss/index.scss';
import Wrapper from './pages/Wrapper/Wrapper';
import {Provider} from 'react-redux';
import {store} from './store/store';

class App extends Component {
  render() {
    return (
          <Provider  store={store}>
            <div className="App">
              <Wrapper/>
            </div>
          </Provider>
    );
  }
}

export default App;
